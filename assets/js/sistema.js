function peticionAjax(url,datos, response){
    $.ajax({
        type: "POST",
        url: url,
        dataType : 'json',
        data: datos,
        success: response,
        error:response           
    });                
}

function validar(e, opt) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla === 8)
        return true; //valida tecla de retroceso
    if (tecla === 0)
        return true; //valida tecla de tabulacion

    switch (opt)
    {
        case 1:
            {
                patron = /[A-Za-zñÑáéíóúÁÉÍÓÚ\d\t]/; //Permite letras y numeros        
            }
            break;
        case 2:
            {
                patron = /[\d\t]/; //Permite solo numeros
            }
            break;
        case 3:
            {
                patron = /[A-Za-zñÑáéíóúÁÉÍÓÚ\t]/; //Permite solo letras
            }
            break;
        case 4:
            {
                patron = /[A-Za-zñÑáéíóúÁÉÍÓÚ\s\t]/; //Permite letras y el espacio en blanco
            }

            break;
        case 5:
            {
                patron = /[A-Za-zñÑáéíóúÁÉÍÓÚ\s\d\t\-]/; //Permite letras, numeros y el espacio en blanco
            }
            break;
        case 6:
            {
                //patron = /[-\d\t]/; //Permite solo numeros y guion
                patron = /[A-Za-zñÑáéíóúÁÉÍÓÚ\d\t\-]/; //Permite letras, numeros y guion
            }
            break;
        case 7:
            {
                patron = /[\d\t\.]/; //Permite solo numeros y punto
            }
            break;
        case 8:
            {
                patron = /[A-Za-zñÑáéíóúÁÉÍÓÚ\t\.\_]/; //Permite letras, guion bajo y punto
            }
            break;
        case 9:
            {
                patron = /[A-Za-zñÑáéíóúÁÉÍÓÚ\t\.\_\/]/; //Permite letras, guion bajo, diagonal y punto
            }
            break;
        case 10:
            {
                patron = /[A-Za-z\d\t]/; //Permite letras sin tildes y numeros        
            }
            break;
        case 11:
            {
                patron = /[\d\t\s]/; //Permite solo numeros y espacio en blanco      
            }
            break;
        case 13:
            {
                patron = /[A-Za-zñÑ\d\t\s\.\,]/; //Permite letras sin tildes, numeros y espacios y ñ  
            }
            break;  
        case 14:
            {
                patron = /[\d\t\.\,]/; //Permite solo numeros coma y punto
            }
            break;        
        default:
        {
            patron = /[\'\?\¡\¿\*\"\~\[\]\{\}\+\$\&\%\#\=\^\<\>\(\)\!]/; //Permite todo menos los caracteres extraños
            te = String.fromCharCode(tecla); // 5
            return !patron.test(te); // 6 
        }
    }

    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6   
};


function error(errores){
    var errors = errores; 
    errorsHtml = '<div><ul>';
    $.each( errors , function( key, value ) {
        errorsHtml += '<li>' + value[0] + '</li>'; 
    });
    errorsHtml += '</ul></div>';
    $('#alerta').delay(1).show(1000);
    $('#alerta').html(errorsHtml).delay(1000).hide(1000); 
}

function aviso(mensaje) {
    $('#aviso').delay(1).show(1000);
    $('#aviso').html(mensaje).delay(1000).hide(1000);
}

function alerta(mensaje) {
    $('#alerta').delay(1).show(1000);
    $('#alerta').html(mensaje).delay(1000).hide(1000);
}
