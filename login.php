<?php
    include('models/Usuario.php');
    session_start();
   
    if($_SERVER["REQUEST_METHOD"] == "POST") { 
        $user = $_POST['usuario'];
        $password = $_POST['password']; 
        $resultado = Usuario::logear($user, $password);
        if(count($resultado) == 1) {
            $_SESSION['usuario_logeado'] = (object) reset($resultado);
            $_SESSION['titulo'] ='Inicio';
            header("location: view/inicio/index.php");
        }else {
            $_SESSION['error_login'] = "Las credenciales estan incorrectas";
            unset($_SESSION['usuario_logeado']);


            header("location: index.php");
        }
    }
?>