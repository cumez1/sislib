<?php
    session_start();
   
    if(isset($_SESSION["error_login"])) { 
        $error = $_SESSION["error_login"];
        unset($_SESSION['error_login']);
    }

    if(isset($_SESSION["usuario_logeado"])){
        header("location: view/inicio/index.php");
    }
?>
<!DOCTYPE html>
<html lang="es" >

<head>
  <meta charset="UTF-8">
  <title>Login - Sislib</title>
      <link rel="stylesheet" href="assets/css/estilo_login.css">
</head>

<body>

  <div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <h2 class="active">Entrar al sistema</h2>

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="assets/img/icon.svg" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form method="POST" action="login.php">
        <div style = "font-size:14px; color:#cc0000; margin-top:10px"><?php echo @$error; ?></div>  
        <input type="text" class="fadeIn second" name="usuario" id="usuario"  placeholder="usuario">
        <input type="password" class="fadeIn third" name="password" id="password" placeholder="contraseña">
        <input type="submit" class="fadeIn fourth" value="Entrar">
    </form>
    
    <!-- Remind Passowrd -->
    <div id="formFooter">
        
    </div>

  </div>
</div>
  
  

</body>

</html>
