<?php

class Helper
{

    public static function string_bd($value)
    {
       
        return "'".trim($value)."'";
    }

    public static function isnull_bd($value)
    {
       	if(trim($value) == '' || trim($value) == null){
       		
          return 'null';
       	}else{

        	return "'".trim($value)."'";
       	}
    }

    public static function dd($value)
    {
     
        echo json_encode($value);
        die();
    }
}
