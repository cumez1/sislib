<?php
    include('../../models/Database.php');
    include('../../helpers/Helper.php');

    session_start();
    $_SESSION['titulo'] ='Clientes';
    
    if(!isset($_SESSION['usuario_logeado'])){
        $_SESSION['error_login'] = "El usuario no esta logueado";
        header("location: ../../index.php");
    }

    $id = $_REQUEST['codigo'];

    $sql = "UPDATE cliente SET estado=0 WHERE id_cliente=$id";


    $con = new Database();
    $result = $con->saveQuery($sql);

    session_start();
    if($result === true){
        $_SESSION['mensaje'] = 'El registro fue dada de baja exitosamente!';
        header("location: index.php");
    }else{
        $_SESSION['mensaje'] = 'Adventencia: '.$result->mensajeOriginal;
        header("location: index.php");
    }
    
?>