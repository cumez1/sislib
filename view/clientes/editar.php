<?php
    session_start();
    $_SESSION['titulo'] ='Clientes';
    
    require_once '../header.php';
    include('../../models/Database.php');
    include('../../helpers/Helper.php');
    if(!isset($_SESSION['usuario_logeado'])){
        $_SESSION['error_login'] = "El usuario no esta logueado";
        header("location: ../../index.php");
    }
    $id = isset($_REQUEST['codigo']) ? $_REQUEST['codigo'] : $_SESSION['mensaje'] = 'El parametro recibo no corresponde al correcto';

    $sql = "SELECT * FROM cliente WHERE id_cliente = ".$id;

    $con = new Database();
    $resultado = $con->executeQuery($sql);

    if(count($resultado)){
    	$cliente = (object) reset($resultado);
    }

    if(isset($_SESSION["mensaje"])) { 
        $mensaje = $_SESSION["mensaje"];
        unset($_SESSION['mensaje']);
    }

?>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<?php require_once('../menu.php'); ?>
	</div>

	<div id="pagina" class="container">
		<div class="title">
			<h2>EDITAR CLIENTE</h2>
		</div>
		<div>
			<h4 style="padding-left: 50px; color: red"> <?php echo @$mensaje; ?></h4>
		</div>

		<div style="padding-left: 50px; padding-bottom: 10px">
			<a href="index.php" class="btn btn-verde"> Regresar</a>
		</div>
		<div>
			<form method="POST" action="actualizar.php">
				
			<table align="center" width="90%">
				<input type="hidden" name="id_cliente" value="<?php echo @$cliente->id_cliente;?>">
				<tr>
					<td >
						<label>NIT: </label><br>
						<input name="nit" id="nit" type="text" value="<?php echo @$cliente->nit; ?>">
					</td>
					<td>
						<label>Primer Nombre: </label><br>
						<input name="nombre1" id="nombre1" type="text" value="<?php echo @$cliente->nombre1 ?>">
					</td>
					<td>
						<label>Segundo Nombre: </label><br>
						<input name="nombre2" id="nombre2" type="text" value="<?php echo @$cliente->nombre2 ?>">
					</td>
					
				</tr>

				<tr>
					<td>
						<label>Primer Apellido: </label><br>
						<input name="apellido1" id="apellido1" type="text" value="<?php echo @$cliente->apellido1 ?>">
					</td>
					<td >
						<label>Segundo Apellido: </label><br>
						<input name="apellido2" id="apellido2" type="text" value="<?php echo @$cliente->apellido2 ?>">
					</td>
					<td>
						<label>Telefono: </label><br>
						<input name="telefono" id="telefono" type="text" value="<?php echo @$cliente->telefono ?>">
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<label>Dirección: </label><br>
						<input name="direccion" id="direccion" type="text" style="width: 90%" value="<?php echo @$cliente->direccion ?>">
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2">
						<label>Correo electronico: </label><br>
						<input name="correo" id="correo" type="text" style="width: 90%" value="<?php echo @$cliente->email ?>">
					</td>
					<td></td>
				</tr>

				<tr>
					<td colspan="3" style="padding-top: 10px;">
						<input type="submit" class="btn btn-verde" value="Actualizar Registro">
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>

</div>
	
	<script src="../../assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		 $("#menu_clientes").addClass("pagina_activa");
		
	</script>
</body>
</html>



