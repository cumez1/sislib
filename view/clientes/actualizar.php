<?php
    session_start();
    $_SESSION['titulo'] ='Clientes';

    include('../../models/Database.php');
    include('../../helpers/Helper.php');

    if(!isset($_SESSION['usuario_logeado'])){
        $_SESSION['error_login'] = "El usuario no esta logueado";
        header("location: ../../index.php");
    }

    $id = $_REQUEST['id_cliente'];
    $nit = Helper::isnull_bd($_REQUEST['nit']);
    $nombre1 = Helper::isnull_bd($_REQUEST['nombre1']);
    $nombre2 = Helper::isnull_bd($_REQUEST['nombre2']);
    $apellido1 = Helper::isnull_bd($_REQUEST['apellido1']);
    $apellido2 = Helper::isnull_bd($_REQUEST['apellido2']);
    $telefono = Helper::isnull_bd($_REQUEST['telefono']);
    $direccion = Helper::isnull_bd($_REQUEST['direccion']);
    $correo = Helper::isnull_bd($_REQUEST['correo']);

    $sql = "UPDATE cliente SET nit=$nit,nombre1=$nombre1,nombre2=$nombre2,apellido1=$apellido1,apellido2=$apellido2,telefono=$telefono,direccion=$direccion,email=$correo WHERE id_cliente=$id";


    $con = new Database();
    $result = $con->saveQuery($sql);

    session_start();
    if($result === true){
        $_SESSION['mensaje'] = 'El registro fue actualizado exitosamente!';
        header("location: index.php");
    }else{
        $_SESSION['mensaje'] = 'Adventencia: '.$result->mensajeOriginal;
        header("location: editar.php?codigo=$id");
    }
    
?>