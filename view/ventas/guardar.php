<?php
    include('../../models/Database.php');
    include('../../helpers/Helper.php');

    session_start();
    if(!isset($_SESSION['usuario_logeado'])){
        $_SESSION['error_login'] = "El usuario no esta logueado";
        header("location: ../../index.php");
    }

    $idcategoria = 1;
    $nit = Helper::isnull_bd($_REQUEST['nit']);
    $nombre1 = Helper::isnull_bd($_REQUEST['nombre1']);
    $nombre2 = Helper::isnull_bd($_REQUEST['nombre2']);
    $apellido1 = Helper::isnull_bd($_REQUEST['apellido1']);
    $apellido2 = Helper::isnull_bd($_REQUEST['apellido2']);
    $telefono = Helper::isnull_bd($_REQUEST['telefono']);
    $direccion = Helper::isnull_bd($_REQUEST['direccion']);
    $correo = Helper::isnull_bd($_REQUEST['correo']);


    $sql = "INSERT INTO cliente (id_categoria, nit, nombre1, nombre2, apellido1, apellido2, direccion, email, telefono) 
            VALUES ($idcategoria, $nit, $nombre1, $nombre2, $apellido1, $apellido2, $direccion, $correo, $telefono)";

    $con = new Database();
    $result = $con->saveQuery($sql);

    session_start();
    if($result === true){
        $_SESSION['mensaje'] = 'El registro fue registrado exitosamente!';
        header("location: index.php");
    }else{
        $_SESSION['mensaje'] = 'Adventencia: '.$result->mensajeOriginal;
        header("location: crear.php");
    }
    
?>