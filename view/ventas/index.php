<?php
	session_start();

    if(!isset($_SESSION['usuario_logeado'])){
    	$_SESSION['error_login'] = "El usuario no esta logueado";
      	header("location: ../../index.php");
   	}
   
    if(isset($_SESSION["mensaje"])) { 
        $mensaje = $_SESSION["mensaje"];
        unset($_SESSION['mensaje']);
    }

    $_SESSION['titulo'] ='Ventas';

    include('../header.php');
    include('../../models/Database.php');
    
    


?>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<?php require_once('../menu.php'); ?>
	</div>

	<div id="pagina" class="container">
		<div class="title">
			<h2>VENTAS</h2>
		</div>
		<div>
			<h4 style="padding-left: 50px; color: green"> <?php echo @$mensaje; ?></h4>
		</div>
		<div style="padding-left: 50px; padding-bottom: 10px">
			<a href="crear.php" class="btn btn-verde"> Crear Nuevo</a>
		</div>
		<div>
			<table align="center" width="90%" class="table">
				<tr>
					<th>Codigo</th>
					<th>NIT</th>
					<th>Nombre Completo</th>
					<th>Dirección</th>
					<th>Telefono</th>
					<th>Acciones</td>
				</tr>
				<tbody>
			<?php  
				$sql = "SELECT * FROM cliente WHERE estado = true";
				$con = new Database();
				$result = $con->executeQuery($sql);
				foreach ($result as $key => $item) {
					$item = (object) $item; 

					$nombre_completo = $item->nombre1.' '.$item->nombre2.' '.$item->apellido1.' '.$item->apellido2;

			?>
				<tr>
					<td> <?php echo ($key +1) ?> </td>
					<td> <?php echo $item->nit ?> </td>
					<td> <?php echo $nombre_completo ?> </td>
					<td> <?php echo $item->direccion ?> </td>
					<td> <?php echo $item->telefono ?> </td>
					<td> 
						<center>
							<?php $url = "editar.php?codigo=".$item->id_cliente; ?>
							<?php $urlE = "eliminar.php?codigo=".$item->id_cliente; ?>
							<a href="<?php echo $url?>" class="btn btn-xs btn-azul"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
							<a href="<?php echo $urlE?>" class="btn btn-xs btn-rojo"> <i class="fa fa-trash-o" aria-hidden="true"></i></a>
						</center>


					</td>
				</tr>
			
			<?php }?>
				</tbody>
			</table>
		</div>
	</div>

</div>
	<?php
    	require_once '../scripts.php';
	?>

	<script type="text/javascript">
		 $("#menu_ventas").addClass("pagina_activa");
		
	</script>
</body>
</html>



