<div id="header" class="container">
	<div id="logo">
		<h1><a href="#">SISLIB</a></h1>
	</div>
	<div id="menu">
		<ul>
			<li id="menu_inicio"><a href="../inicio/index.php" accesskey="1" title="">INICIO</a></li>
			<li id="menu_clientes" ><a href="../clientes/index.php" accesskey="2" title="">CLIENTES</a></li>
			<li id="menu_proveedores" ><a href="../proveedores/index.php" accesskey="3" title="">PROVEEDORES</a></li>
			<li id="menu_productos" ><a href="#" accesskey="4" title="">PRODUCTOS</a></li>
			<li id="menu_ventas" ><a href="../ventas/index.php" accesskey="5" title="">VENTAS</a></li>
			<li id="menu_compras" ><a href="#" accesskey="6" title="">COMPRAS</a></li>
			<li id="menu_reportes" ><a href="#" accesskey="7" title="">REPORTES</a></li>
		</ul>
	</div>
</div>