<?php
    session_start();
    $_SESSION['titulo'] ='Inicio';

    require_once '../header.php';
    if(!isset($_SESSION['usuario_logeado'])){
    	$_SESSION['error_login'] = "El usuario no esta logueado";
      	header("location: ../../index.php");
   	}
?>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<?php require_once('../menu.php'); ?>
	</div>

	<div id="extra" class="container">
		<div class="title">
			<h2>LIBRERIA LA ASUNCIÓN</h2>
			<span class="byline">Sistema de control de productos, ventas y compras</span> </div>
		<div id="three-column">
			<center>
				<img src="../../assets/img/logo.jpg" alt="Logo de la libreria" height="250px;">
			</center>
		</div>
	</div>

</div>
	<?php require_once('../footer.php'); ?>
	<script src="../../assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		 $("#menu_inicio").addClass("pagina_activa");
		
	</script>
</body>
</html>



