<?php
    session_start();
    $_SESSION['titulo'] ='Proveedores';
    
    require_once '../header.php';
    include('../../models/Database.php');
    include('../../helpers/Helper.php');
    if(!isset($_SESSION['usuario_logeado'])){
        $_SESSION['error_login'] = "El usuario no esta logueado";
        header("location: ../../index.php");
    }
    $id = isset($_REQUEST['codigo']) ? $_REQUEST['codigo'] : $_SESSION['mensaje'] = 'El parametro recibo no corresponde al correcto';

    $sql = "SELECT * FROM proveedor WHERE id_proveedor = ".$id;

    $con = new Database();
    $resultado = $con->executeQuery($sql);

    if(count($resultado)){
    	$proveedor = (object) reset($resultado);
    }

    if(isset($_SESSION["mensaje"])) { 
        $mensaje = $_SESSION["mensaje"];
        unset($_SESSION['mensaje']);
    }

?>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<?php require_once('../menu.php'); ?>
	</div>

	<div id="pagina" class="container">
		<div class="title">
			<h2>EDITAR PROVEEDOR</h2>
		</div>
		<div>
			<h4 style="padding-left: 50px; color: red"> <?php echo @$mensaje; ?></h4>
		</div>

		<div style="padding-left: 50px; padding-bottom: 10px">
			<a href="index.php" class="btn btn-verde"> Regresar</a>
		</div>
		<div>
			<form method="POST" action="actualizar.php">
				
			<table align="center" width="90%">
				<input type="hidden" name="id_proveedor" value="<?php echo @$proveedor->id_proveedor;?>">
				<tr>
					<td colspan="2">
						<label>NIT: </label><br>
						<input name="nit" id="nit" type="text" style="width: 90%" value="<?php echo @$proveedor->nit; ?>">
					</td>
					<td colspan="2">
						<label>Nombre del proveedor: </label><br>
						<input name="proveedor" id="proveedor" type="text" style="width: 90%" value="<?php echo @$proveedor->proveedor ?>">
					</td>
					
				</tr>

				
				<tr>
					<td colspan="2">
						<label>Dirección: </label><br>
						<input name="direccion_comercial" id="direccion_comercial" type="text" style="width: 90%" value="<?php echo @$proveedor->direccion_comercial ?>">
					</td>
					<td colspan="2">
						<label>Telefono: </label><br>
						<input name="telefono" id="telefono" style="width: 90%" type="text" value="<?php echo @$proveedor->telefono ?>">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label>Correo electronico: </label><br>
						<input name="email" id="email" type="text" style="width: 90%" value="<?php echo @$proveedor->email ?>">
					</td>
					<td colspan="2"></td>
				</tr>

				<tr>
					<td colspan="3" style="padding-top: 10px;">
						<input type="submit" class="btn btn-verde" value="Actualizar Registro">
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>

</div>
	
	<script src="../../assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		 $("#menu_proveedores").addClass("pagina_activa");
		
	</script>
</body>
</html>



