<?php
    session_start();
    $_SESSION['titulo'] ='Proveedores';
    
    include('../../models/Database.php');
    include('../../helpers/Helper.php');

    if(!isset($_SESSION['usuario_logeado'])){
        $_SESSION['error_login'] = "El usuario no esta logueado";
        header("location: ../../index.php");
    }

    $id = $_REQUEST['id_proveedor'];
    $nit = Helper::isnull_bd($_REQUEST['nit']);
    $proveedor = Helper::isnull_bd($_REQUEST['proveedor']);
    $direccion_comercial = Helper::isnull_bd($_REQUEST['direccion_comercial']);
    $telefono = Helper::isnull_bd($_REQUEST['telefono']);
    $email = Helper::isnull_bd($_REQUEST['email']);

    $sql = "UPDATE proveedor SET nit=$nit,proveedor=$proveedor,direccion_comercial=$direccion_comercial,telefono=$telefono,email=$email WHERE id_proveedor=$id";


    $con = new Database();
    $result = $con->saveQuery($sql);

    session_start();
    if($result === true){
        $_SESSION['mensaje'] = 'El registro fue actualizado exitosamente!';
        header("location: index.php");
    }else{
        $_SESSION['mensaje'] = 'Adventencia: '.$result->mensajeOriginal;
        header("location: editar.php?codigo=$id");
    }
    
?>