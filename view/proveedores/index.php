<?php
    session_start();
    $_SESSION['titulo'] ='Proveedores';
    require_once '../header.php';
    include('../../models/Database.php');

    if(!isset($_SESSION['usuario_logeado'])){
    	$_SESSION['error_login'] = "El usuario no esta logueado";
      	header("location: ../../index.php");
   	}
   
    if(isset($_SESSION["mensaje"])) { 
        $mensaje = $_SESSION["mensaje"];
        unset($_SESSION['mensaje']);
    }

?>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<?php require_once('../menu.php'); ?>
	</div>

	<div id="pagina" class="container">
		<div class="title">
			<h2>PROVEEDORES</h2>
		</div>
		<div>
			<h4 style="padding-left: 50px; color: green"> <?php echo @$mensaje; ?></h4>
		</div>
		<div style="padding-left: 50px; padding-bottom: 10px">
			<a href="crear.php" class="btn btn-verde"> Crear Nuevo</a>
		</div>
		<div>
			<table align="center" width="90%" class="table">
				<tr>
					<th>Codigo</th>
					<th>NIT</th>
					<th>Nombre Completo</th>
					<th>Dirección</th>
					<th>Telefono</th>
					<th>Acciones</td>
				</tr>
				<tbody>
			<?php  
				$sql = "SELECT * FROM proveedor WHERE estado = true";
				$con = new Database();
				$result = $con->executeQuery($sql);
				foreach ($result as $key => $item) {
					$item = (object) $item; 
			?>
				<tr>
					<td> <?php echo ($key +1) ?> </td>
					<td> <?php echo $item->nit ?> </td>
					<td> <?php echo $item->proveedor ?> </td>
					<td> <?php echo $item->direccion_comercial ?> </td>
					<td> <?php echo $item->telefono ?> </td>
					<td> 
						<center>
							<?php $url = "editar.php?codigo=".$item->id_proveedor; ?>
							<?php $urlE = "eliminar.php?codigo=".$item->id_proveedor; ?>
							<a href="<?php echo $url?>" class="btn btn-xs btn-azul"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
							<a href="<?php echo $urlE?>" class="btn btn-xs btn-rojo"> <i class="fa fa-trash-o" aria-hidden="true"></i></a>
						</center>


					</td>
				</tr>
			
			<?php }?>
				</tbody>
			</table>
		</div>
	</div>

</div>
	
	<script src="../../assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		 $("#menu_proveedores").addClass("pagina_activa");
		
	</script>
</body>
</html>



