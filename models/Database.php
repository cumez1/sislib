<?php

include('config.php');

/**
 * Calse de conexion mediante PDO
 */
class Database
{
    /**
     *
     *@return $pdo Conexion
     */
    public function conectar()
    {
        $host = DB_HOST;
        $port = DB_PORT;
        $db = DB_DATABASE;
        $user = DB_USERNAME;
        $pass = DB_PASSWORD;

        $pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public function executeQuery($sql){
        try {
            $result = array();

            $pdo = self::conectar();
            $stm = $pdo->prepare($sql);
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_ASSOC);
        } catch(Exception $e) {
            //Obtener mensaje de error.
            die($e->getMessage());
        }
    }

    public function saveQuery($sql){
        try {


            $pdo = self::conectar();
            $stm = $pdo->prepare($sql);

            if ($stm) {
                $stm->execute();
                $result = TRUE;
            } else {
                $result = FALSE;
            }

            return $result;

        } catch(Exception $e) {

            $error = $e->errorInfo;
            $resp['SQLSTATE'] = $error[0];
            $resp['constrant'] = $error[1];
            $resp['mensaje'] = $error[2];
            $resp['mensajeOriginal'] = $e->getMessage();
            $error = (object) $resp;

            return $error;
        }
    }
}
